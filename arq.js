const btnenviar = document.querySelector(".enviar")
const btnexcluir = document.querySelector(".excluir")
const bloco = document.querySelector(".bloco")

btnenviar.addEventListener("click", () =>{
    let input = document.querySelector(".textarea")
    const mensagem = document.createElement("p")
    mensagem.classList.add("msg")
    mensagem.innerText = input.value
    
    const conteiner = document.createElement("div")
    const divButton = document.createElement("div")
    const botaoEditar = document.createElement("button")
    const botaoExcluir = document.createElement("button")
    conteiner.classList.add("conteiner-msg")
    divButton.classList.add("buttons")
    botaoEditar.classList.add("edit")
    botaoExcluir.classList.add("excluir")

    botaoEditar.innerText = "Editar"
    botaoExcluir.innerText = "Excluir"
    botaoExcluir.setAttribute("onclick","excluir(this)")

    divButton.appendChild(botaoEditar)
    divButton.appendChild(botaoExcluir)
   
    conteiner.appendChild(mensagem)
    conteiner.appendChild(divButton)

    bloco.appendChild(conteiner)
    
    
})

 function excluir(elemento){
   elemento.parentNode.parentNode.remove()
 }